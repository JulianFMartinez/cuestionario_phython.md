**1. ¿Cómo tienes instalado Python? Podéis acompañarlo de la salida de which python, which python3, python --version, python3 --version**

Phython fue instalado en las sesiones de noviembre del módulo I y se hizo por la terminal. 

Al ejecutar which python sale:

/Users/apple/opt/anaconda3/bin/python

Con which python3, aparece:

/Users/apple/opt/anaconda3/bin/python3

La versión instalada es:

Python 3.9.7

**2. ¿Tienes Anaconda? ¿Lo usas fuera del máster o/y en el máster? Puedes acompañarlo de echo $PATH**

Anaconda fue instalado en el ordenador pero no desde la terminal. Por medio del programa se abre JupyterLab. El uso de Anaconda ha sido para los ejercicios del master. 

**3. ¿Ejecutas Python desde la terminal? Con la salida de which hazle un ls -la a esa ruta. Aquí me interesa saber si ejecutas Jupyter desde la terminal, desde la aplicación gráfica de Anaconda o cómo.**

Solamente he usado el programa Anaconda.

**4. ¿Usas Collab? ¿Solo collab o como una aplicación de terceros más?**

No conozco qué es Collab.

**5. ¿Sabes que es pip? Puedes acompañarlo de pip --version y pip3 --version, which pip y which pip3. Por cierto que en mi GNU/Linux me dice que which`está depreciado, que use `command -v, es decir, command -v pip, etc.**

No sé qué es pip, pero al ejecutar which pip en la terminal aparece: 

/Users/apple/opt/anaconda3/bin/pip

pip --version

pip 21.2.4 from /Users/apple/opt/anaconda3/lib/python3.9/site-packages/pip (python 3.9)


**6. ¿Usas pip, condas u otro método?**

Ninguno. 
